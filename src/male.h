#ifndef MALE_H
#define MALE_H

# include "individual.h"


class Male: public Individual {

protected:

	ent conquests;

public:

	// Constructor & destructor
	Male ( const Genetic_parameters& gp ) : Individual (gp) { gender = true; conquests = 0; initialize_traits(gp); }
	~Male () {};

	// Get methods
	bool ask_gender () const { return true; }
	ent ask_Nmates () const { return conquests; }
	std::list<Individual*> get_sperm () { throw_error(ERROR_INDIV_MOMFEMA); return {}; }

	// Individual-interactivity
	void add_conquest () { ++conquests; }

};

#endif
