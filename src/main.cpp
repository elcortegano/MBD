// Code used in López-Cortegano et al. (2019) Similarity-based mate choice on body size in sexually dimorphic populations causes string sexual selection

// Software to simulate random mating with mate choice
// in populations with preference bias and sexual dimorphism

#include "main.h"

int main (int argc, char** argv) {

	// LET THERE BE LIGHT
	const clock_t begin_time = clock();
	Settings settings (argc, argv);
	rnd_generator.seed (settings.get_seed());
	std::cout << "seed: " << settings.get_seed() << std::endl;

	// GET SETTING PARAMETERS
	Population_parameters pop_info (settings.get_pop_parameters());
	ent iterations (settings.get_iter());
	Matechoice_parameters mc (settings.get_mc_parameters ());
	Genetic_parameters genetics (settings.get_genetic_parameters());

	// SUMMON INDIVIDUALS
	std::cout << "Running simulation..." << std::endl;
	Population population (pop_info, genetics, mc);
	population.log_individuals();

	// SIMULATION OVER t MATING SEASONS
	do {
		do {
			population.tryhard_mating ();
		} while (!population.season_ends(iterations));
	} while (iterations);

	// THE END
	std::cout << "Done! Execution took " << float( clock () - begin_time ) /  CLOCKS_PER_SEC << " seconds." << std::endl;

	return 0;
}
