#ifndef MCHOICE_H
#define MCHOICE_H

#include "alias.h"
#include "settings.h"
#include <list>


// =====   GLOBAL, EXTERN & STATIC VARIABLES   =====
extern std::mt19937 rnd_generator;

class Population;
class Individual;


// ====   CLASSES   ====
class MChoice {

protected:

	Matechoice_parameters mc_info; // mate choice basic settings
	coefficient tolerance;
	bool calc_f; // preference values must be calculated from phenotypes
	coefficient D_max; // maximum phenotypic difference
	Matrix f_matrix; // matrix of preference values
	coefficient f_max; // maximum preference

public:

	// Constructor & destructor
	MChoice () {};
	MChoice ( Population& , const Matechoice_parameters& );
	void init ( Population& , const Matechoice_parameters& );
	void update ( Population& );

	// Preference models
	bool match ( Individual* , Individual* );

	// Get methods
	Matechoice_parameters get_mc () const { return mc_info; }
	coefficient get_Dmax () const { return D_max; }

	// Set methods
	void set_bias (coefficient b) { mc_info.b = b; }

protected:

	// Mate choice parameters
	void calculate_Dmax ( Population& );

	// Preference models
	coefficient generic ( coefficient , coefficient , coefficient );
	coefficient calc_gaussian_preference ( Individual* , Individual* );
	coefficient alpha_FND ( coefficient , coefficient );
	void calculate_f_matrix ( Population& );
	void calculate_fmax ( Population& );

};

#endif
