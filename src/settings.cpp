/* =======================================================================================
 *                                                                                       *
 *       Filename: settings.cpp                                                          *
 *                                                                                       *
 *    Description: Functions to parse and set program options.                           *
 *                                                                                       *
 ======================================================================================= */

#include "settings.h"

/* ***************************************************************************************
 * Constructor & destructor
 * ***************************************************************************************/
Settings::Settings (int argc, char** argv) {

	// Set init default values
	parameters.seed = (int)time(0);
	parameters.i = DEFAULT_I;
	pop_info.N_inds = DEFAULT_N;
	mc_info.b = DEFAULT_B;
	mc_info.preference = DEFAULT_PREFERENCE;
	mc_info.choosy = DEFAULT_CHOOSY;
	genetics.size.Va = DEFAULT_VA;
	genetics.size.mean_fem = DEFAULT_MEAN_SIZE;
	genetics.size.mean_mal = DEFAULT_MEAN_SIZE;
	genetics.choice = DEFAULT_MEAN_C;

	// Set control variables
	set_dist_size = false;
	set_dist_size_fem = false;
	set_dist_size_mal = false;
	set_dist_C = false;
	call_simple_C = false;
	call_simple_size = false;
	call_gender_size = false;

	// Parse complex arguments
	std::vector<std::string> arguments;
	for (int i(0); i<argc; ++i) arguments.push_back(std::string(argv[i]));

	int new_argc (arguments.size());
	char ** new_argv = new char* [new_argc];
	for (int i(0); i<new_argc; ++i) {
		new_argv[i] = new char [arguments[i].size()+1];
		strcpy(new_argv[i], arguments[i].c_str());
	}

	// Local variables
	int c;

	while (true) {

		int option_index (0);
		c = getopt_long (new_argc, new_argv, "s:i:N:C:b:", long_options, &option_index);

		if (c == -1) break;

		switch (c) {

			case 's':
			parameters.seed = atoi(optarg);
			break;

			case 'i':
			parameters.i = atoi(optarg);
			break;

			case 'C':
			genetics.choice = atof(optarg);
			call_simple_C = true;
			break;

			case 'b':
			mc_info.b = atof(optarg);
			break;

			case 'N':
			pop_info.N_inds = atoi(optarg);
			break;

			case 0:
			if (long_options[option_index].flag != 0) break;
			else if (strcmp(long_options[option_index].name, "choosy-males") == 0) {
				if (mc_info.choosy != DEFAULT_CHOOSY) throw_error (ERROR_SETNG_TOOCHOS);
				mc_info.choosy = MALES;
			}
			else if (strcmp(long_options[option_index].name, "Va") == 0) {
				genetics.size.Va = atof(optarg);
			} else if (strcmp(long_options[option_index].name, "size") == 0) {
				if (call_gender_size) throw_error (ERROR_SETNG_DIMORPH);
				genetics.size.mean_fem = atof(optarg);
				genetics.size.mean_mal = genetics.size.mean_fem;
				call_simple_size = true;
			} else if (strcmp(long_options[option_index].name, "size.fem") == 0) {
				if (call_simple_size) throw_error (ERROR_SETNG_DIMORPH);
				genetics.size.mean_fem = atof(optarg);
				call_gender_size = true;
			} else if (strcmp(long_options[option_index].name, "size.mal") == 0) {
				if (call_simple_size) throw_error (ERROR_SETNG_DIMORPH);
				genetics.size.mean_mal = atof(optarg);
				call_gender_size = true;
			} else if (strcmp(long_options[option_index].name, "size.Va") == 0) genetics.size.Va = atof(optarg);
			break;

			case '?':
			throw_error (ERROR_SETNG_UNKNOWN);

			default:
			throw_error (ERROR_SETNG_UNKNOWN);

		}
	}

	// Error for non-option arguments
	if (optind < new_argc) {
		std::string extra_args (new_argv[optind++]);
		while (optind < new_argc) {
			extra_args += " ";
			extra_args += (new_argv[optind++]);
		}
		throw_error(ERROR_SETNG_NONARGV, extra_args);
	}

	// Conditional default values
	if (parameters.i==0) throw_error (ERROR_SETNG_ZEROGEN);
	for (int i (0); i < new_argc; i++) delete[] new_argv[i];
	delete[] new_argv;
}
