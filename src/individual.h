#ifndef INDIVIDUAL_H
#define INDIVIDUAL_H


#include <list>
#include "alias.h"
#include "error.h"
#include "settings.h"
#include "genetics.h"

// =====   GLOBAL, EXTERN & STATIC VARIABLES   =====
static ent ids = 0;
class Trait;
extern std::mt19937 rnd_generator;


// ====   CLASSES   ====
class Individual {

friend class Trait;

protected:

	ent name; // unique ID for the individual (required?)
	bool gender; // 0 female, 1 male
	bool enough; // enough sex?
	Trait size; // body size
	coefficient C; // choice parameter

public:

	// Constructor & destructor
	Individual ( const Genetic_parameters& );
	virtual ~Individual () {};

	// Get methods
	ent ask_name () const { return name; };
	virtual bool ask_gender () const =0;
	coefficient ask_size () const { return size.value_P(); }
	coefficient ask_choice () const { return C; }

	// Set methods
	void set_size (double s) { size.setP (s);};

	// Individual-interactivity
	virtual void mate ( Individual* ) {}
	virtual void add_conquest () {}

protected:

	// Get methods
	Trait trait_size () const { return size; }
	coefficient trait_C () const { return C; }
	virtual std::list<Individual*> get_sperm () =0;

	// Set methods
	void initialize_traits ( const Genetic_parameters& );

};

#endif
