#ifndef FEMALE_H
#define FEMALE_H

# include "individual.h"

class Female: public Individual {

protected:

	std::list<Individual*> spermathecal;

public:

	// Constructor & destructor
	Female ( const Genetic_parameters& gp ) : Individual (gp) { gender = false; initialize_traits(gp); }
	~Female () {};

	// Get methods
	bool ask_gender () const { return false; }
	ent ask_Nmates () const { return spermathecal.size(); }
	bool is_pregnant () { if (spermathecal.size()) return true; else return false; }
	std::list<Individual*> get_sperm () { return spermathecal; }

	// Individual-interactivity
	void mate ( Individual* );

};

#endif
