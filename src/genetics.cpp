#include "genetics.h"
#include "individual.h"
#include "population.h"

/* ***************************************************************************************
 * Constructor & destructor
 * ***************************************************************************************/
/* Trait: Initializes a trait from probability distributions.
 * --------------------------------------------------------------------------------------*/
Trait::Trait (const Trait_parameters& t, bool gender) {
	// Genetic value
	if (gender) dist = Gaussian (t.mean_mal, sqrt(t.Va));
	else dist = Gaussian (t.mean_fem, sqrt(t.Va));
	A = dist (rnd_generator);
	// Phenotype
	compute_phenotype ();
}
