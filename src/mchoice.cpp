#include "mchoice.h"
#include "population.h"


/* ***************************************************************************************
 * Constructor & destructor
 * ***************************************************************************************/

/* MChoice: Initializes the suite of mating choice parameters.
 * --------------------------------------------------------------------------------------*/
MChoice::MChoice (Population& pop, const Matechoice_parameters& mc_info) {
	init (pop, mc_info);
}

/* init: This function initializes all basic parameters for mate choice. It is aimged to be
 * called once by mating season
 * --------------------------------------------------------------------------------------*/
void MChoice::init (Population& pop, const Matechoice_parameters& mc) {
	mc_info = mc;
	tolerance = 0.1;
	calc_f = true;
	calculate_Dmax(pop);
	D_max = 1.0;
	if (calc_f) calculate_f_matrix (pop);
	this->update (pop);
}

/* update: Update some mate choice parameters (eg. parameters that depend only on available
 * individuals and need to be updated after every mating round
 * --------------------------------------------------------------------------------------*/
void MChoice::update (Population& pop) {
	if (calc_f) calculate_fmax (pop);
}

/* ***************************************************************************************
 * Mate choice parameters
 * ***************************************************************************************/

/* calculate_Dmax: Calculates the maximum phenotypic difference
 * --------------------------------------------------------------------------------------*/
void MChoice::calculate_Dmax (Population& pop) {
	coefficient min_f (pop.get_ind(0)->ask_size());
	ent N (pop.inquire_N());
	ent N_ladies (pop.inquire_Nladies());
	coefficient max_f (min_f);
	for (ent i(1); i<N_ladies; ++i) {
		if (pop.get_ind(i)->ask_size()<min_f) min_f = pop.get_ind(i)->ask_size();
		else if (pop.get_ind(i)->ask_size()>max_f) max_f = pop.get_ind(i)->ask_size();
	}
	coefficient min_m (pop.get_ind(N_ladies)->ask_size());
	coefficient max_m (min_m);
	for (ent i(N_ladies+1); i<N; ++i) {
		if (pop.get_ind(i)->ask_size()<min_m) min_m = pop.get_ind(i)->ask_size();
		else if (pop.get_ind(i)->ask_size()>max_m) max_m = pop.get_ind(i)->ask_size();
	}

	if (abs(max_f-min_m) > abs(min_f-max_m)) D_max = abs(max_f-min_m);
	else D_max = abs(min_f-max_m);
}

/* ***************************************************************************************
 * Preference models
 * ***************************************************************************************/

/* match: Calculates probability of mating given a preference vlaue
 * --------------------------------------------------------------------------------------*/
bool MChoice::match (Individual* i, Individual* j) {
	if (f_max==0.0) return 1.0;
	coefficient preference;
	preference = calc_gaussian_preference(i,j);
	preference /= f_max;
	coefficient U = sample_prob();
	if (U<=preference) return true;
	else return false;
}

/* generic: General preference function that gives the propensity of mating between an
 * individuai i with another j
 * --------------------------------------------------------------------------------------*/
coefficient MChoice::generic (coefficient alpha0, coefficient alpha1, coefficient Z_ij) {
	return exp(alpha0 + alpha1*Z_ij);
}

/* calc_gaussian_preference: Template for gaussian preference models
 * --------------------------------------------------------------------------------------*/
coefficient MChoice::calc_gaussian_preference ( Individual* i, Individual* j ) {
	coefficient alpha0 (0.0);
	coefficient s (tolerance);
	coefficient C (j->ask_choice()); // choosy males
	if ((C < i->ask_choice() && mc_info.choosy==BOTH)) C = i->ask_choice();
	if (C > 1.0) C = 1.0;
	else if (C < -1.0) C = -1.0;

	// Calculating alpha1
	coefficient alpha1 (0.0);
	alpha1 = alpha_FND (C,s);

	// Calculating Z_ij
	coefficient D_ij (abs(i->ask_size() - j->ask_size()));
	if (mc_info.choosy != BOTH) D_ij = i->ask_size() - j->ask_size();
	D_max = 1.0;
	coefficient b (D_max);
	if (C>=0.0) b = mc_info.b; // assortative positive mating
	coefficient Z_ij ((D_ij-b)*(D_ij-b)) ;
	Z_ij /= (D_max*D_max);

	return generic (alpha0, alpha1, Z_ij);
}

/* alpha_FND: Regression term alpha1 in FND gaussian model
 * --------------------------------------------------------------------------------------*/
coefficient MChoice::alpha_FND (coefficient C, coefficient s) {
	return (-pow(C/s,2.0));
}

/* calculate_f_matrix: Calculates the matrix of preference values
 * --------------------------------------------------------------------------------------*/
void MChoice::calculate_f_matrix (Population& pop) {
	ent N (pop.inquire_N());
	ent N_ladies (pop.inquire_Nladies());
	f_matrix.clear();
	for (ent i(0); i<N_ladies; ++i) {
		Vector f_row;
		for (ent j(N_ladies); j<N; ++j) {
			f_row.push_back(calc_gaussian_preference(pop.get_ind(i),pop.get_ind(j)));
		}
		f_matrix.push_back(f_row);
	}
}

/* calculate_fmax: Calculates the maximum preference value among all available pairs of
 * indviduals
 * --------------------------------------------------------------------------------------*/
void MChoice::calculate_fmax (Population& pop) {
	ent N_ladies (pop.inquire_Nladies());
	if (!f_matrix.size() || (f_matrix.size() != N_ladies)) throw_error (ERROR_MCHOI_MAXPREF);
	f_max = 0.0;
	for (ent i(0); i<f_matrix.size(); ++i) {
		for (ent j(0); j<f_matrix[i].size(); ++j) {
				if (f_matrix[i][j]>f_max) f_max = f_matrix[i][j];
		}
	}
}
