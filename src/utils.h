#ifndef UTILS_H
#define UTILS_H

#include "alias.h"
#include "error.h"
#include <list>
#include <algorithm>
#include <cstring>
#include <sstream>


// =====   GLOBAL, EXTERN & STATIC VARIABLES   =====
extern std::mt19937 rnd_generator;


/* ***************************************************************************************
 * Template functions
 * ***************************************************************************************/

/* kill: Free memory and clear standard containers.
 * --------------------------------------------------------------------------------------*/
template<typename T>
inline void kill (std::vector<T>& v) {
	for (auto& i: v) delete i;
	v.clear();
}

template<typename T>
inline void kill (std::list<T>& l) {
	for (auto& i: l) delete i;
	l.clear();
}

/* sample: Sample values from vector or numeric sequences
 * --------------------------------------------------------------------------------------*/
template<typename T>
inline T sample (const std::vector<T>& v) {
	if (v.size()>1) {
		int_Unif dist (0, v.size()-1);
		return v[dist(rnd_generator)];
	} else {
		return v[0];
	}
}

template<typename T>
T sample (std::list<T>& v) {
	typename std::list<T>::iterator it (v.begin());
	if (v.size()) {
		int_Unif dist (0, v.size()-1);
		std::advance(it,dist(rnd_generator));
	}
	return *it;
}

/* is_in: Checks if a value is contained in a vector
 * --------------------------------------------------------------------------------------*/
template<typename T>
inline bool is_in (std::vector<T> v, T x) {
	if (!v.size()) return false;
	for (auto& i: v) if (i==x) return true;
	return false;
}

/* diff: Returns the elements of vector1 that are not present in vector2
 * --------------------------------------------------------------------------------------*/
template<typename T>
inline void diff (std::vector<T>& v1, const std::vector<T>& v2) {
	if (!v1.size() || !v2.size()) return;
	for (ent i(v1.size()-1); i>= 0; --i) {
		for (ent j(0); j<v2.size(); ++j) {
			if (v1[i] == v2[j]) {
				v1.erase(v1.begin()+i);
				break;
			}
		}
		if (!i) break;
	}
}

/* split_string: Splits an string into a vector of strings, given a delimiter
 * --------------------------------------------------------------------------------------*/
inline std::vector<std::string> split_string (std::string string_, char delim) {
	std::vector<std::string> result;
	std::istringstream iss (string_);
	std::string piece;
	while (getline(iss, piece, delim)) result.push_back(piece.c_str());
	return result;
}


/* ***************************************************************************************
 * Generic inline functions
 * ***************************************************************************************/

/* mean: calculates the mean of a numeric vector.
 * --------------------------------------------------------------------------------------*/
inline coefficient mean (Vector v) {
	coefficient sum (0.0);
	for (ent i (0); i < v.size(); ++i) { sum += v[i]; }
	return sum/v.size();
}

/* variance: calculates the variance of a numeric vector.
 * --------------------------------------------------------------------------------------*/
inline coefficient variance (Vector v) {
	coefficient var (0.0);
	coefficient avg (mean(v));
	for (ent i (0); i < v.size(); ++i) { var += ((v[i] - avg) * (v[i] - avg)); }
	return var/v.size();
}

/* isNumeric: True if i>j. Checks if a string looks like a number or not.
 * --------------------------------------------------------------------------------------*/
inline bool isNumeric(const std::string& input) {
	std::size_t n_point = std::count(input.begin(), input.end(), '.');
	if (n_point==1 && input[0]=='.') throw_error (ERROR_SETNG_NUM_BDO);
	else if (n_point==1 && input[input.size()-1]=='.') throw_error (ERROR_SETNG_NUM_EDO);
	else if (n_point>1) throw_error (ERROR_SETNG_NUM_DOT);
	std::size_t n_minus = std::count(input.begin(), input.end(), '-');
	if (n_minus>1) throw_error (ERROR_SETNG_NUM_MIN);
	return (strspn (input.c_str(), "-.0123456789")==input.size());
}

/* higher_than: True if i>j. Usually called for sorting algorithms.
 * --------------------------------------------------------------------------------------*/
inline bool higher_than ( ent i, ent j) { return (i>j); }

/* sucession: Calculates the third in a sucession of cartesian coordinates, zero is out
 * --------------------------------------------------------------------------------------*/
inline ent sucession (ent first, ent second) {
	if (first==second) return first;
	else if (second>first) return ++second;
	else if (second<first && second) return --second;
	else return second;
}

/* sequence: Returns a sequence of numbers form zero to N-1
 * --------------------------------------------------------------------------------------*/
inline iVector sequence (ent N) {
	iVector seq;
	for (ent i(0); i<N; ++i) seq.push_back(i);
	return seq;
}

/* from_*to: Creation of integer vectors with sequential values
 * --------------------------------------------------------------------------------------*/
inline void from_to (iVector& v, ent min, ent max) {
	v.clear();
	v.reserve(max);
	ent n(min);
	std::generate_n(std::back_inserter(v), max, [n]()mutable { return n++; });
}

inline void from_zero_to (iVector& v, ent max) {
	from_to (v, 0, max);
}

/* sample_from_*: samples a value in an uniform distribution
 * --------------------------------------------------------------------------------------*/
inline ent sample_from_to (const ent& init, const ent& end) {
	int_Unif dist (init, end);
	return dist(rnd_generator);
}

inline ent sample_from_zero_to (const ent& x) {
	return sample_from_to(0,x);
}

/* sample_binomial: Sample values from a binomial distribution
 * --------------------------------------------------------------------------------------*/
inline ent sample_binomial (const ent& N, coefficient p) {
	Binomial dist (N,p);
	return dist(rnd_generator);
}

/* sample_ptob: Samples a probability value between [0,1) from an uniform distribution
 * --------------------------------------------------------------------------------------*/
inline coefficient sample_prob () {
	real_Unif dist (0.0,1.0);
	return dist(rnd_generator);
}

/* set_index_vector: Initializes a vector of size N with values from 0 to N-1
 * --------------------------------------------------------------------------------------*/
inline Vector set_index_vector (ent N) {
	Vector v;
	for (ent i(0); i<N; ++i) v.push_back(i);
	return v;
}

/* ***************************************************************************************
 * Helper functions
 * ***************************************************************************************/

/* print functions: Return the values in a vector or matrix
 * --------------------------------------------------------------------------------------*/
template<typename T>
void pvector (const std::vector<T>& v) {
	for (auto const& i: v) {
		std::cout << i << " " ;
	}	std::cout << std::endl;
}

template<typename T>
void pvector (const std::list<T>& v) {
	for (auto const& i: v) {
		std::cout << i << " " ;
	}	std::cout << std::endl;
}

template<typename T>
void pvector (const std::deque<T>& v) {
	for (auto const& i: v) {
		std::cout << i << " " ;
	}	std::cout << std::endl;
}

template<typename T>
void pmatrix (const std::vector<std::vector<T>>& m) {
	for (auto const& v: m) {
	for (auto const& i: v) {
		std::cout <<  i << " " ;
	}   std::cout << std::endl     ; }
}

template<typename T>
void pmatrix (const std::list<std::list<T>>& m) {
	for (auto const& v: m) {
	for (auto const& i: v) {
		std::cout <<  i << " " ;
	}   std::cout << std::endl     ; }
}

template<typename T>
void pmatrix (const std::deque<std::deque<T>>& m) {
	for (auto const& v: m) {
	for (auto const& i: v) {
		std::cout <<  i << " " ;
	}   std::cout << std::endl     ; }
}

#endif
