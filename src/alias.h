#ifndef ALIAS_H
#define ALIAS_H

#include <vector>
#include <list>
#include <deque>
#include <string>
#include <random>
#include <iostream>


// ============   ALIAS    ============

typedef double               coefficient;
typedef std::vector<coefficient>  Vector;
typedef std::vector<std::string> sVector;
typedef std::size_t ent                 ;
typedef std::vector<ent>         iVector;
typedef std::vector<bool>        bVector;
typedef std::vector<Vector>       Matrix;
typedef std::vector<sVector>     sMatrix;
typedef std::vector<iVector>     iMatrix;
typedef std::vector<bVector>     bMatrix;
typedef std::list<coefficient>      List;
typedef std::list<List>           List2D;
typedef std::deque<coefficient>     Deck;
typedef std::deque<Deck>          Deck2D;
typedef std::vector <Vector*>    pVector;
typedef std::vector <iVector*>  piVector;
typedef std::vector <Matrix*>    pMatrix;
typedef std::vector <iMatrix*>  piMatrix;
typedef std::string                 word;
typedef unsigned              individual;

typedef std::bernoulli_distribution Bernouilli;
typedef std::binomial_distribution<ent> Binomial;
typedef std::normal_distribution<coefficient> Gaussian;
typedef std::uniform_real_distribution<coefficient> real_Unif;
typedef std::uniform_int_distribution<ent> int_Unif;

// ======   GLOBAL VARIABLES    ======
const std::string TAB = "\t";
const std::string SPACE = " ";
const std::string EMPTY = "";
const std::string INT = "INT";
const std::string NUM = "NUM";
const std::string TYPE = "TYPE";

#endif
