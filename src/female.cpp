#include "female.h"
#include "genetics.h"

/* mate: Female is inseminated by a male.
 * --------------------------------------------------------------------------------------*/
void Female::mate (Individual* male) {
	if (this->ask_gender() || (!male->ask_gender())) throw_error (ERROR_INDIV_HOMOSEX);
	male->add_conquest();
	spermathecal.push_back(male);
}
