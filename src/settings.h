#ifndef SETTINGS_H
#define SETTINGS_H

#include <iostream>
#include <getopt.h>
#include <cstring>
#include <algorithm>
#include <fstream>
#include "alias.h"
#include "error.h"
#include "utils.h"


// =======   AVAILABLE OPTIONS  =======
static struct option long_options[] = {
	// *name, has_arg, *flag, val
	// general options
	{"seed",		required_argument,	0,	's'},
	{"iterations",	required_argument,	0,	'i'},
	// population options
	{"Pop-size",	required_argument,	0,	'N'},
	// biological traits
	{"Va",			required_argument,	0,	0},
	{"size",		required_argument,	0,	0},
	{"size.fem",	required_argument,	0,	0},
	{"size.mal",	required_argument,	0,	0},
	{"size.Va",		required_argument,	0,	0},
	// mate choice
	{"choice",		required_argument,	0,	'C'},
	{"choosy-males",no_argument,		0,	0},
	{"bias",		required_argument,	0,	'b'},
	{0,0,0,0}
};

// ====   STRUCTS   ====

struct General_parameters {
	ent seed;
	ent i;
};

struct Population_parameters {
	ent N_inds; // population size
};

enum MMC {
	SIZE,
	RANDOM,
};

enum PModel {
	GAUSSIAN_FND
};

enum choosy_t {
	MALES,
	BOTH
};

struct Matechoice_parameters {
	coefficient b;
	PModel preference;
	choosy_t choosy;
};

struct Trait_parameters {
	coefficient Va;
	coefficient mean_fem;
	coefficient mean_mal;
};

struct Genetic_parameters {
	coefficient choice;
	Trait_parameters size;
};


// =======   DEFAULT VALUES  =======

constexpr ent DEFAULT_I = 5;
constexpr ent DEFAULT_N = 3;
constexpr coefficient DEFAULT_MEAN_C = 0.0;
constexpr coefficient DEFAULT_B = 0.0;
constexpr PModel DEFAULT_PREFERENCE = GAUSSIAN_FND;
constexpr choosy_t DEFAULT_CHOOSY = BOTH;
constexpr coefficient DEFAULT_VA = 1.0;
constexpr coefficient DEFAULT_MEAN_SIZE = 5.0;


// ====   CLASSES   ====
class Settings {

protected:

	// General options
	General_parameters parameters;

	// Parameters
	Population_parameters pop_info;
	Matechoice_parameters mc_info;
	Genetic_parameters genetics;

	// Control variables for complex options
	bool set_dist_size;
	bool set_dist_size_fem;
	bool set_dist_size_mal;
	bool set_dist_C;
	bool call_simple_C;
	bool call_simple_size;
	bool call_gender_size;

public:

	Settings ( int , char** );

	// Get methods
	ent get_seed () { return parameters.seed; }
	ent get_iter () { return parameters.i; }
	Population_parameters get_pop_parameters () { return pop_info; }
	Matechoice_parameters get_mc_parameters () { return mc_info; }
	Genetic_parameters get_genetic_parameters () { return genetics; }

};

#endif
