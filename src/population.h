#ifndef POPULATION_H
#define POPULATION_H

#include <algorithm>
#include "alias.h"
#include "utils.h"
#include "individual.h"
#include "female.h"
#include "male.h"
#include "settings.h"
#include "mchoice.h"


// =====   GLOBAL, EXTERN & STATIC VARIABLES   =====
extern std::mt19937 rnd_generator;

// ====  STRUCTS   ====
struct Pedigree_record {
	ent ind;
	coefficient size;
	ent t;
};
typedef std::deque<Pedigree_record> Pedigree;

struct Registry {
	ent ID_female;
	ent ID_male;
	coefficient P_female;
	coefficient P_male;
	ent t;
	bool success;
};


// ====  INLINE FUNCTIONS   ====
inline Pedigree_record ind_to_ped (Individual* ind, ent t) {
	Pedigree_record ped;
	ped.ind = ind->ask_name();
	ped.size = ind->ask_size();
	ped.t = t;
	return ped;
}

// ====   CLASSES   ====
class Population {

protected:

	ent generation;
	std::vector<Individual*> population;
	ent N;
	ent N_ladies;
	Pedigree pedigree; // genealogical records of the parents of current population
	std::vector<Registry> registry;
	ent nmatings;
	Population_parameters parameters;
	Genetic_parameters pop_genetics;

	// Mate choice parameters
	MChoice pop_mchoice;
	coefficient b0;

public:

	// Constructor & destructor
	Population ( const Population_parameters& , const Genetic_parameters& , const Matechoice_parameters& );
	~Population ();

	// Individuals activity
	void tryhard_mating ();

	// Environmental methods
	bool season_ends ( ent& );

	// Get methods
	Individual* get_ind ( ent index ) { return population[index]; }
	ent inquire_N () const { return N; }
	ent inquire_Nladies () const { return N_ladies; }
	ent inquire_size ( ent ind ) const { return population[ind]->ask_size(); }

	// Set methods
	void log_individuals ();
	void log_matings ( Individual* , Individual* , bool );

	// Save methods
	void survey ();
	void save_pedigree ();
	void save_registry ();

protected:

	// Individuals activity
	void random_population ( std::vector<Individual*>& , ent& );

	// Set methods
	void set_new_population ( const std::vector<Individual*>& , ent );
};

#endif
