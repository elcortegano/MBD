#include "individual.h"
#include "genetics.h"
#include "utils.h"

/* ***************************************************************************************
 * Constructor & destructor
 * ***************************************************************************************/

/* Individual: Initializes an individual with random parameters.
 * --------------------------------------------------------------------------------------*/
Individual::Individual (const Genetic_parameters& genetics) {
	name = ++ids;
	enough = false;
}

/* initialize_traits: Initialize the genetic and phenotypic value of biological traits
 * --------------------------------------------------------------------------------------*/
void Individual::initialize_traits (const Genetic_parameters& gp) {
	size = Trait (gp.size, gender);
	C = gp.choice;
}
