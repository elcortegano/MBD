#ifndef GENETICS_H
#define GENETICS_H

#include "alias.h"
#include "settings.h"


// =====   FORWARD DECLARATIONS   =====
class Individual;
struct Pedigree_record;

// =====   EXTERN VARIABLES   =====
extern std::mt19937 rnd_generator;


// ====   CLASSES   ====
class Trait { // It represents an additive trait

protected:

	Gaussian dist; // trait distribution
	coefficient A; // additive value
	coefficient P; // phenotypic value

public:

	Trait () {};
	Trait ( const Trait_parameters& , bool );
	coefficient value_P () const { return P; }
	void setP (double p) {P=p;}

protected:

	void compute_phenotype () { P = A; }
};

#endif
