#ifndef MAIN_H
#define	MAIN_H

#include <iostream>
#include <time.h>
#include "alias.h"
#include "population.h"
#include "settings.h"
#include "genetics.h"


// =====   GLOBAL VARIABLES   =====
std::mt19937 rnd_generator;
Bernouilli sample_bool;

#endif
