#include "population.h"
#include "genetics.h"

/* ***************************************************************************************
 * Constructor & destructor
 * ***************************************************************************************/
Population::Population ( const Population_parameters& pop_info, const Genetic_parameters& genetics, const Matechoice_parameters& mc_info) {

	// Determine number of males, females and migrants
	generation = 0;
	N = pop_info.N_inds;
	pop_genetics = genetics;

	// Generate the population
	random_population (population, N_ladies);

	// Set other population parameters
	parameters = pop_info;
	//n_old_members = 0;
	nmatings = 0;

	// Initialize mate choice parameters
	// scale
	double min_size (population[0]->ask_size());
	double max_size (min_size);
	for (std::size_t i(1); i<N; ++i) {
		if (population[i]->ask_size() < min_size) min_size = population[i]->ask_size();
		if (population[i]->ask_size() > max_size) max_size = population[i]->ask_size();
	}
	double orange (max_size-min_size);
	double mintrait (0.0);
	double Dmax (1.0);
	for (std::size_t i(0); i<N; ++i) {
		population[i]->set_size((Dmax*(population[i]->ask_size()-min_size)/(orange)) + mintrait);
	}
	b0 = mc_info.b;
	pop_mchoice.set_bias (Dmax*(b0)/(orange));
	pop_mchoice.init(*this, mc_info);

}

Population::~Population () {
	kill (population);
}

/* tryhard_mating: This functions loops over all females (or males) and forces them to
 * encounter to every male (or female) until there is a mate, or until all pairs are rejected.
 * --------------------------------------------------------------------------------------*/
void Population::tryhard_mating () {

	ent eval_males (0);
	for (ent i(N_ladies); i<N; ++i) {
		Individual* male (population[i]);
		iVector female_list;
		from_zero_to(female_list, N_ladies-1);
		shuffle(std::begin(female_list), std::end(female_list), rnd_generator);
		bool mate (false);
		ent eval_females (0);
		while (!mate && female_list.size()) {
			ent fem_index (female_list[female_list.size()-1]);
			Individual* female (population[fem_index]);
			if (pop_mchoice.match(female, male)) {
				female->mate(male);
				log_matings (female, male, true);
				mate = true;
			} else {
				log_matings (female, male, false);
				female_list.pop_back();
			}
			++eval_females;
			if (eval_females>=500) break;
		}
		++eval_males;
		if (eval_males>=100) break;
	}

}

/* random_population: Generates a random population of females and males.
 * --------------------------------------------------------------------------------------*/
void Population::random_population ( std::vector<Individual*>& pop, ent& N_fem) {

	// Get a number of females
	N_fem = sample_binomial(N,0.5);
	while (!N_fem || N_fem ==N) {
		N_fem = sample_binomial(N,0.5);
	}
	N_fem = N*0.5;
	// Create population vector. Ladies first
	for (ent i(0); i<N_fem; ++i) {
		pop.push_back(new Female (pop_genetics));
	}
	for (ent i(N_fem); i<N; ++i) {
		pop.push_back(new Male (pop_genetics));
	}
}

/* season_ends: Season ends when all females have mate (or tried it?) at least X times with
 * males. Individuals will then have offspring, and a new population will be generated.
 * --------------------------------------------------------------------------------------*/
bool Population::season_ends (ent& remaining_iterations) {

	// Save population files
	save_registry ();
	save_pedigree ();

	// If season ends, a new generation of individuals borns
	std::vector<Individual*> new_population;
	ent N_daughters (0);
	random_population (new_population, N_daughters);

	// Set new population
	set_new_population (new_population, N_daughters);
	--remaining_iterations;

	// Recalculate mate choice parameters
	double min_size (population[0]->ask_size());
	double max_size (min_size);
	for (std::size_t i(1); i<N; ++i) {
		if (population[i]->ask_size() < min_size) min_size = population[i]->ask_size();
		if (population[i]->ask_size() > max_size) max_size = population[i]->ask_size();
	}
	double orange (max_size-min_size);
	double mintrait (0.0);
	double Dmax (1.0);
	for (std::size_t i(0); i<N; ++i) {
		population[i]->set_size((Dmax*(population[i]->ask_size()-min_size)/(orange)) + mintrait);
	}
	pop_mchoice.set_bias (Dmax*(b0)/(orange));
	if (remaining_iterations) pop_mchoice.init(*this, pop_mchoice.get_mc());
	return true;
}

/* ***************************************************************************************
 * Set methods
 * ***************************************************************************************/

/* set_new_population: Gets a vector of new individuals and set a new population.
 * --------------------------------------------------------------------------------------*/
void Population::set_new_population (const std::vector<Individual*>& new_pop, ent N_daughters) {
	log_individuals();
	++generation;
	kill (population);
	population = { std::make_move_iterator(std::begin(new_pop)), std::make_move_iterator (std::end(new_pop)) };
	N = population.size();
	N_ladies = N_daughters;
}

/* log_individuals: Appends basic individuals information into a pedigree record.
 * --------------------------------------------------------------------------------------*/
void Population::log_individuals () {
	Pedigree new_pedigree;
	for (auto& ind: population) {
		Pedigree_record ped (ind_to_ped(ind, generation));
		new_pedigree.push_back(ped);
	}
	pedigree = new_pedigree;
}

/* log_matings: Records matings into a civil registry.
 * --------------------------------------------------------------------------------------*/
void Population::log_matings (Individual* female, Individual* male, bool mated) {
	Registry reg;
	reg.t = generation;
	reg.ID_female = female->ask_name();
	reg.ID_male = male->ask_name();
	reg.P_female = female->ask_size();
	reg.P_male = male->ask_size();
	reg.success = mated;
	registry.push_back(reg);
	if (mated) ++nmatings;
}

/* ***************************************************************************************
 * Save methods
 * ***************************************************************************************/

/* survey: Prints basic information about individuals in the population (for checking
 * purposes).
 * --------------------------------------------------------------------------------------*/
void Population::survey () {
	static ent count (0);
	std::cout << "Population survey call " << ++count << std::endl;
	for (ent i(0); i<N; ++i) {
		std::cout << "ind (" << population[i]->ask_name()
				  << "), gender " << population[i]->ask_gender()
				  << std::endl;
	}
}

/* save_pedigree: Saves an output pedigree file.
 * --------------------------------------------------------------------------------------*/
void Population::save_pedigree () {

	static ent count (0);

	// Save header
	std::string filename ("pedigree.csv");
	std::ofstream outfile;
	if (!count) {
		outfile.open(filename);
		outfile << "ind,gender,size,t";
		outfile << std::endl;
	} else outfile.open(filename, std::ios::app);

	// Save pedigree
	for (auto& ind: population) {
		outfile << ind->ask_name() << ','
				<< ind->ask_gender() << ','
				<< ind->ask_size();
		outfile << ',' << generation;
		outfile << std::endl;
	}
	outfile.close();

	// Clear pedigree
	++count;
}

/* save_registry: Saves an output pedigree file.
 * --------------------------------------------------------------------------------------*/
void Population::save_registry () {

	static ent count (0);

	// Save header
	std::string filename ("registry.csv");
	std::ofstream outfile;
	if (!count) {
		outfile.open(filename);
		outfile << "female,fsize,male,msize,t" << std::endl;
	} else outfile.open(filename, std::ios::app);

	// Save registry
	for (ent i(0); i<registry.size(); ++i) {
		if (registry[i].success) {
			outfile << registry[i].ID_female << ','
				<< registry[i].P_female << ','
				<< registry[i].ID_male << ','
				<< registry[i].P_male << ','
				<< registry[i].t;
			outfile << std::endl;
		}
	}
	outfile.close();

	// Clear registry
	registry.clear();
	nmatings = 0;
	++count;
}
