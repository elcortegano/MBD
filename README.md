# MBD

**A software to simulate random mating with Mate choice in populations with preference Bias and sexual Dimorphism**

## Overview

The code in this repository was used for publication in:

- López-Cortegano, E.; Carpena, C.; Carvajal-Rodríguez, A.; Rolán-Álvarez, E. (2019) *Similarity-based mate choice on body size in sexually dimorphic populations causes strong sexual selection*.

It is modified from an in-development software (hopefully to be published in the future) and current changes made in this repository respond to the need to adequate that code to the use in the above mentioned publication.

Please, cite it if you use MBD or code within in your research.

## Installation

You can download MBD from the terminal by entering:

```
git clone https://gitlab.com/elcortegano/MBD.git
```

It can be compiled from source by using `make`.

```
cd src/
make
```

## Use

The program can be used to simulate matings with preference bias and sexual size dimorphism. For example:

```
./mate-trace --choosy-males -i 1000 -s 1234 -N 1000 -C 0.5 --size.mal 0.0 --size.fem 0.5 --bias 0.25
```

The above indicates that 1000 iterations (`-i`) will be run using seed number 1234 (`-s`). For each of these, 1000 individuals (`-N`) i.e. 500 males and 500 females are going to be simulated and mated. The distribution of males body size is N (mean=0, var=1) (given by options `--size.mal` and `--size.Va.mal`). For females it is N (0.5, 1). The mate choice parameter is 0.5 (`-C`), and the preference bias is 0.25 (`-b`). Choice is exerced by males (`--choosy-males`). Other details on the mating model are described in the article mentioned above.

This will generate two files: **pedigree.csv** with individual phenotypic data, and **registry.csv**, with information of all encounters simulated and the succeded mates. These information can be used to compute in each case, the sexual selection or the phenotypic correlation between mating pairs.

## Assistance

Please use the [issue tracker](https://gitlab.com/elcortegano/MBD/issues) to submit requests or troubles with the code.
